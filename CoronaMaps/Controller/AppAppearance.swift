//
//  AppAppearance.swift
//  CoronaMaps
//
//  Created by PiPyL on 3/9/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import Foundation
import UIKit

class AppAppearance {
    
    static func setAppAppearance() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16, weight: .semibold)];
        
        UINavigationBar.appearance().backIndicatorImage = UIImage.init(named: "ic_back")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage.init(named: "ic_back")
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for: UIBarMetrics.default)
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.init(hex: "5ca2d6")
        UINavigationBar.appearance().shadowImage = UIImage()
    }
}

