//
//  AppDataSingleton.swift
//  FFL
//
//  Created by Jude on 9/11/18.
//  Copyright © 2018 FFL. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

enum Defaults {
    static func set(_ object: Any, forKey defaultName: String) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(object, forKey:defaultName)
        defaults.synchronize()
        
    }
    static func object(forKey key: String) -> AnyObject! {
        let defaults: UserDefaults = UserDefaults.standard
        return defaults.object(forKey: key) as AnyObject?
    } }

class AppDataSingleton: NSObject {
    static let sharedInstance = AppDataSingleton()
    var dateFormatter = DateFormatter()
    var checkShowTimerView = false
    var bottomPadding:CGFloat = 0
    
    var ref: DatabaseReference!
    
    override init() {
        super.init()
        dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone.init(identifier: "UTC")
        ref = Database.database().reference()
    }
}
