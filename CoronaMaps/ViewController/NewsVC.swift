//
//  NewsVC.swift
//  CoronaMaps
//
//  Created by PiPyL on 3/9/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

class NewsVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Tin Tức"
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)

        
        if let url = URL.init(string: "https://ncov.moh.gov.vn/web/guest/-ieu-can-biet") {
            let request = URLRequest.init(url: url)
            webView.load(request)
        }
    }
    

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loading" {
            if webView.isLoading {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }

}
