//
//  MenuVC.swift
//  CoronaMaps
//
//  Created by PiPyL on 3/9/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class MenuVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.init(hex: "5ca2d6")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StatictisVC")
            let navi = UINavigationController.init(rootViewController: vc!)
            self.present(navi, animated: true, completion: nil)
            break
        case 2:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SolutionVC")
            let navi = UINavigationController.init(rootViewController: vc!)
            self.present(navi, animated: true, completion: nil)
            break
        case 3:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewsVC")
            let navi = UINavigationController.init(rootViewController: vc!)
            self.present(navi, animated: true, completion: nil)
            break
        default:
            break
        }
    }
}
