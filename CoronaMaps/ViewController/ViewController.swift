//
//  ViewController.swift
//  CoronaMaps
//
//  Created by PiPyL on 3/8/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import MBProgressHUD
import SideMenuSwift

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var detailView: UIView!
    
    var locationManager = CLLocationManager()
    var isToCurrentLocation = true
    private var currentLocation: CLLocation?
    
    var listProvince = [Province]()
    var listLocation = [Location]()
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preapreUI()
        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SideMenuController.preferences.basic.menuWidth = 250
    }
    
    //MARK: - Private
    
    private func preapreUI() {
        
        self.navigationItem.title = "Corona Maps"
        // Get current location
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        // Check for Location Services
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        let leftBarButton = UIBarButtonItem.init(image: UIImage.init(named: "ic_menu"), style: .done, target: self, action: #selector(didClickMenu))
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    @objc private func didClickMenu() {
        sideMenuController?.revealMenu()
    }
    
    private func setupData() {
        
//        currentLocation = CLLocation.init(latitude: 16.061867, longitude: 108.228367)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Province.sharedInstance.fetchAll { [weak self] (listProvinces, error) in
            
            MBProgressHUD.hide(for: self!.view, animated: true)
            if let list = listProvinces {
                self?.listProvince = list
                
                for location in list {
                    for location1 in location.location {
                        self?.listLocation.append(location1)
                        
                        DispatchQueue.main.async {
                            self?.addPoint(location: location1)
                        }
                    }
                }
            }
        }
    }
    
    private func addPoint(location: Location) {
        let annotation = MKPointAnnotation()
        let center = CLLocationCoordinate2D(latitude: location.lat, longitude: location.longField)
        annotation.coordinate = center
        annotation.title = location.name
        if let detail = location.detail {
            annotation.subtitle = detail
        }
        self.mapView.addAnnotation(annotation)
    }
    
    //MARK: - Action
    
    @IBAction func didClickClose(_ sender: Any) {
        detailView.isHidden = true
    }
    @IBAction func didClickCurrentLocation(_ sender: Any) {
        isToCurrentLocation = true
    }
}

//MARK: - MapKit Delegate

extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = true
        }
        else {
            anView!.annotation = annotation
        }
        
        if annotation.coordinate.latitude != currentLocation?.coordinate.latitude || annotation.coordinate.longitude != currentLocation?.coordinate.longitude {
            anView!.image = UIImage(named: "ic_virus")
        } else {
            anView!.image = UIImage(named: "ic_star")
        }
        
//        if annotation.coordinate.latitude != locationManager.location?.coordinate.latitude || annotation.coordinate.longitude != locationManager.location?.coordinate.longitude {
//            anView!.image = UIImage(named: "ic_virus")
//        } else {
//            anView!.image = UIImage(named: "ic_star")
//        }
        
        return anView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let anotation = view.annotation, let subTilte = anotation.subtitle {
            if let detail = subTilte, detail != "" {
                detailLabel.text = detail
                detailView.isHidden = false
                return
            }
        }
        detailView.isHidden = true
    }
}

//MARK: - CLLocationManager Delegate

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last, isToCurrentLocation == true {
            currentLocation = CLLocation.init(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.mapView.setRegion(region, animated: true)
            isToCurrentLocation = false
        }
    }
}


