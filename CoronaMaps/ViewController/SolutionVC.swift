//
//  SolutionVC.swift
//  CoronaMaps
//
//  Created by PiPyL on 3/9/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class SolutionVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Biện Pháp Phòng Ngừa"
    }

    // MARK: - Table view
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.size.width * 1.742
    }
}
